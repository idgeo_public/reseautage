### Le réseautage en géomatique

![](ressources/rezo.jpg)

La notion de réseautage, celle qui permet d'étendre son réseau professionnel, est un aspect très présent dans le milieu de la géomatique.

Le résautage va se décliner de nombreuses façons :

    . savoir être
    . s'ouvrir à autre chose
    . participer à des événements extra-métier
    . devenir acteur au sein de réseaux
    . s'investir grâce à son travail vs passion chronophage
    . équilibre entre têtes dans le guidon et ouverture vers l'extérieur

Le réseautage, pourquoi ?

    . s'épanouir personnellement
    . ne pas s'endormir dans sa discipline
    . participer à de plus grands projets
    . rencontrer du monde
    ...

Comment faire du réseautage en géomatique ?

    . connaitre l'environnement professionel
    . connaitre l'environnement technique

*Certains disent que le milieu de la géomatique est un petit milieu !*

Bien que l'aspect concurrentiel soit présent ici comme ailleurs, il y a de nombreuses facilités pour faire converger les énergies.

La géomatique est un être vivant et en pleine croissance.

### Environnement professionel

Petit rappel sur le paysage actuel vu dans les bases des SIG.

Une organisation qui évolue avec la discipline (et vice vers ça) => OPEN DATA

> Le SIG dans les institutions. 

> Le SIG dans les entreprises privées.

> Le SIG propriétaire et Open Source.

**Des organismes institutionels :**

- l'IGN https://ign.fr/carto
- l'AFIGEO https://www.afigeo.asso.fr/ (Idgeo est membre)
 - le CNIG http://cnig.gouv.fr/
- Data.gouv https://www.data.gouv.fr/fr/

Qu'est-ce que les CRIGE ? https://www.youtube.com/watch?v=BxnpuCk3dvc (Idgeo est membre du OpenIG)

**Des Associations :**

- Georezo.net avec ses forums technique et emploi qui cartonnent https://georezo.net/
- Osgeo section france : https://www.osgeo.asso.fr/ (Idgeo est membre)

**Des Groupes thématiques**

- cf Commissions du CNIG qui rassemblent décideurs et partenaires privés => Grace THD


### Environnement technique

> Logiciels bureautique => on peut résumer la situation ArcGIS / Qgis

> Solutions WEB - SIG => Solution clé en main / solution à mettre en place soit même

**Les données** : l'accés à la donnée s'améliore mais c'est pas encore gagné.

Vidéo de capitaine moustache Moustache ! https://www.youtube.com/watch?v=jqyfvyFaA1Y

**Le développement** : un gros mot qui n'en est pas un !

On peut devenir contributeur QGIS en traduisant la documentation ou participer à l'animation de l'association pour faire vivre la communauté.

### Les communautés

Il y en a plusieurs, un peu trop ou plutôt n'y en aurait-il pas qu'une ?

Le méli mélo des utilisateurs, développeurs, traducteurs, modérateurs ...

### Les événements ...

Les géodatadays => https://www.youtube.com/watch?v=QXoDb94GxN4 (Idgeo participe en tant que soutien et exposant)

Forum annuel ESRI => SIG 2020 ...

Les QGIS Open Days tous les derniers vendredi du mois => https://github.com/qgis/QGIS/wiki#qgisopenday-previously-known-as-qhackfriday

Les reclus aux confins (assez récent) => https://www.twitch.tv/confins

### De la presse spécialisées

https://decryptageo.fr

https://static.geotribu.fr/ (Idego a déjà participé)

### Les RS

Deux incontournables

![Twitter](ressources/twitter.png)

![LinkedIn](ressources/linkedin.png)

### Des lignes supplémentaires sur le CV

Participer à des réseaux pro est à valoriser sur son CV. Il s'agit de critères qui sont de plus en plus regardés par les recruteurs !
